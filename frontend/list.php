<?php
    if (!defined('APP')) die();

    include_once "frontend/templates/userslist.php";
?>

<div class="row pt-2" id="addUserForm">
    <div class="col">
        <form class="border p-2" style="background: #f5f5f5">
            <div class="form-group">
                <label for="fullname">full name</label>
                <input type="text" class="form-control" id="fullname"   placeholder="Jon Snow">
            </div>
            <div class="form-group">
                <label for="birth date">birth date</label>
                <input type="text" class="form-control" id="birthdate" placeholder="format: 1990-01-27">
            </div>
            <button type="button" id="addUserBn" class="btn btn-primary">CREATE NEW USER</button>
        </form>
    </div>
</div>

<div class="row pt-2">
    <div class="col">
        <div id="usersTable">
        loading...
        </div>
    </div>
</div>

<script>
    function loadUsers() {
        $.get("backend/api.php?action=GetAllUsers", function(data) {
            var tpl = $("#usersTableTpl").html();

            $("#usersTable").html(Mustache.render(tpl, {users: data}));

            $(".deleteUser").click(function() {
                var userId = $(this).attr("dataUserId");
                
                $.post("backend/api.php?action=DeleteUser", {user_id: userId}, function(resp) {
                    if (resp.ok) {
                        loadUsers();
                    } else {
                        alert(resp.error);
                    }
                });
                return false;
            })
        });
    }

    $(document).ready(function() {
        loadUsers();

        $("#addUserBn").click(function() {
            var user = {
                full_name: $("#fullname").val(),
                birth_date: $("#birthdate").val()
            };
            
            $.post("backend/api.php?action=AddUser", user, function(resp) {
                if (resp.ok) {
                    loadUsers();
                } else {
                    alert("add user error");
                }
            });
        })
    });
</script>