<?php
    if (!defined('APP')) die();

    $user_id = (int)$_GET['id'];
    $user_full_name = $_GET['fn'];
    $user_birth_date = $_GET['bd'];

    include_once "frontend/templates/addresses.php"
?>

<div class="row pt-2" id="saveUserForm">
    <div class="col">
        <form id="updateUser" class="border p-2" style="background: #f5f5f5">
            <input type="hidden" id="user_id" value="<?= $user_id ?>"/>
            <div class="form-group">
                <label for="fullname">full name</label>
                <input type="text" class="form-control" id="fullname" value="<?= $user_full_name ?>">
            </div>
            <div class="form-group">
                <label for="birth date">birth date</label>
                <input type="text" class="form-control" id="birthdate" value="<?= $user_birth_date ?>">
            </div>
            <button type="submit" class="btn btn-primary">SAVE</button>
        </form>
    </div>
</div>

<div class="row pt-2">
    <div class="col" id="userDataTable">
        loading...
    </div>
</div>

<div class="row pt-2">
    <div class="col">
        <button id='createUserData' class="btn btn-primary">CREATE USER DATA</button>
    </div>
</div>

<div id="userDataModal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">User data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="userDataForm">

      <div class="modal-body">
            <input type="hidden" id="user_id" value="<?= $user_id ?>"/>
            <input type="hidden" id="user_data_id" value=""/>

            <div class="form-group">
                <label for="phone">phone</label>
                <input type="text" class="form-control" id="phone" value="">
            </div>
            <div class="form-group">
                <label for="address">address</label>
                <input type="text" class="form-control" id="address" value="">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="text" class="form-control" id="email" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
    var userData = null;

    function getUserDataById(id) {
        if (!userData) {
            return null;
        }

        for (var i=0; i<userData.length; i++) {
            if (userData[i].id == id) {
                return userData[i];
            }
        }
        return null;
    }

    function loadUserData() {
        $.get("backend/api.php?action=GetUserData&user_id=<?= $user_id ?>", function(resp) {
            userData = resp;
            var tpl = $("#userDataTpl").html();
            
            $("#userDataTable").html(Mustache.render(tpl, {data: userData}));

            $(".editUserData").click(function() {
                var id = $(this).attr('dataId');
                var it = getUserDataById(id);
                
                if (!it) {
                    alert("user data not found: "+id);
                    return;
                }
                
                $("#user_data_id").val(it.id);
                $("#user_id").val(it.user_id);
                $("#phone").val(it.phone);
                $("#address").val(it.address);
                $("#email").val(it.email);

                $("#userDataModal").modal('show');
                return false;
            });

            $(".deleteUserData").click(function() {
                var id = $(this).attr("dataId");

                $.post("backend/api.php?action=DeleteUserData", {data_id: id}, function(resp) {
                    if (resp.ok) {
                        loadUserData();
                    } else {
                        alert(resp.error);
                    }
                });
                return false;
            })
        });
    }

    $(document).ready(function() {
        loadUserData();

        $("#updateUser").submit(function() {
            var params = {
                id: $("#user_id").val(),
                full_name: $("#fullname").val(),
                birth_date: $("#birthdate").val()
            };
            
            $.post("backend/api.php?action=UpdateUser", params, function(resp) {
                if (resp.ok) {
                    alert("save ok")
                } else {
                    alert(ersp.error);
                }
            });
            return false;
        });

        $("#userDataForm").submit(function() {
            var params = {
                id: $("#user_data_id").val(),
                user_id: $("#user_id").val(),
                phone: $("#phone").val(),
                address: $("#address").val(),
                email: $("#email").val()
            };

            if (params.id) {
                $.post("backend/api.php?action=UpdateUserData", params, function(resp) {
                    if (resp.ok) {
                        $("#userDataModal").modal('hide');
                        loadUserData();
                    } else {
                        alert(resp.error);
                    }
                })
            } else {
                $.post("backend/api.php?action=CreateUserData", params, function(resp) {
                    if (resp.ok) {
                        $("#userDataModal").modal('hide');
                        loadUserData();
                    } else {
                        alert(resp.error);
                    }
                })
            } 
            return false;
        })

        $("#createUserData").click(function() {
            $("#userDataModal").modal('show');
        });
    });
</script>


