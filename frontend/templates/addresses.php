<script id="userDataTpl" type="x-tmpl-mustache">
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>phone</th>
                <th>address</th>
                <th>email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {{#data}}
            <tr>
                <td>{{id}}</td>
                <td>{{phone}}</td>
                <td>{{address}}</td>     
                <td>{{email}}</td>
                <td>
                    <a dataId="{{id}}" class="editUserData" href="#">[ edit ]</a>
                    <a dataId="{{id}}" class="deleteUserData" href="#">[ delete ]</a>
                </td>
            </tr>
            {{/data}}
        </tbody>
    </table>
</script>