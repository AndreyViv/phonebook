<script id="usersTableTpl" type="x-tmpl-mustache">
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>full name</th>
                <th>birth date</th>
            </tr>
        </thead>
        <tbody>
            {{#users}}
            <tr>
                <td>{{id}}</td>
                <td>{{full_name}}</td>
                <td>{{birth_date}}</td>     
                <td>
                    <a dataUserId="{{id}}" class="editUser" href="index.php?page=user&id={{id}}&fn={{full_name}}&bd={{birth_date}}">[ edit ]</a>
                    <a dataUserId="{{id}}" class="deleteUser" href="#">[ delete ]</a>
                </td>
            </tr>
            {{/users}}
        </tbody>
    </table>
</script>