<?php

include_once "models/User.php";
include_once "models/UserData.php";

class Api {
    public function __construct($db) {
        $this->db = $db;
    }

    public function run() {
        if (!isset($_GET['action'])) {
            return;
        }
        
        $meth = 'handle'.$_GET['action'];
        
        if (!method_exists($this, $meth)) {
            return;
        }
        
        $this->$meth();
    }

    private function sendJSON($obj) {
        header('Content-type: application/json');
        echo json_encode($obj);
    }

    private function handleGetAllUsers() {
        $m = new User($this->db);
        $users = $m->getAllUsers();
        
        $this->sendJSON($users);
    }

    private function handleAddUser() {
        $m = new User($this->db);

        $status = $m->createUser([
            'full_name' => $_POST['full_name'],
            'birth_date' => $_POST['birth_date']
        ]);
        
        if ($status) {
            $this->sendJSON(["ok" => 1]);
        } else {
            $this->sendJSON(['error' => 'create user failed']);
        }
    }

    private function handleDeleteUser() {
        $user_id = (int)$_POST['user_id'];
        $m = new User($this->db);

        $status = $m->deleteUser($user_id);
        
        if ($status) {
            $this->sendJSON(["ok" => 1]);
        } else {
            $this->sendJSON(['error' => 'delete user failed']);
        }
    }

    private function handleUpdateUser() {
        $m = new User($this->db);

        $status = $m->updateUser($_POST);
        
        if ($status) {
            $this->sendJSON(["ok" => 1]);
        } else {
            $this->sendJSON(['error' => 'update user failed']);
        }
    }

    private function handleGetUserData() {
        $user_id = (int)$_GET['user_id'];
        $m = new UserData($this->db);
        
        $data_list = $m->getDataList($user_id);
        
        $this->sendJSON($data_list);
    }

    private function handleDeleteUserData() {
        $data_id = (int)$_POST['data_id'];
        $m = new UserData($this->db);

        $status = $m->deleteUserData($data_id);
        
        if ($status) {
            $this->sendJSON(['ok' => 1]);
        } else {
            $this->sendJSON(['error' => 'delete user data failed']);
        }
    }

    private function handleCreateUserData() {
        $m = new UserData($this->db);

        $status = $m->createUserData($_POST);
        
        if ($status) {
            $this->sendJSON(['ok' => 1]);
        } else {
            $this->sendJSON(['error' => 'create user data failed']);
        }
    }

    private function handleUpdateUserData() {
        $m = new UserData($this->db);

        $status = $m->updateUserData($_POST);
        
        if ($m->updateUserData($_POST)) {
            $this->sendJSON(['ok' => 1]);
        } else {
            $this->sendJSON(['error' => 'update user data failed']);
        }
    }
}