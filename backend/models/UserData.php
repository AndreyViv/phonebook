<?php

class UserData {
    public function __construct($db) {
        $this->db = $db;
    }

    public function getDataList($user_id) {
        $stmt = $this->db->prepare('SELECT * FROM user_data WHERE user_id = :user_id');
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSingleData($user_id, $id) {
        $stmt = $this->db->prepare('SELECT * FROM user_data WHERE user_id = :user_id and id = :id');
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function createUserData($params) {
        $stmt = $this->db->prepare('INSERT INTO user_data (phone, address, email, user_id) VALUES (:phone, :address, :email, :user_id)');
        
        $stmt->bindParam(':phone', $params['phone']);
        $stmt->bindParam(':address', $params['address']);
        $stmt->bindParam(':email', $params['email']);
        $stmt->bindParam(':user_id', $params['user_id']);

        return $stmt->execute();
    }

    public function updateUserData($params) {
        $stmt = $this->db->prepare('UPDATE user_data SET phone = :phone, address = :address, email = :email WHERE id = :id');
        $stmt->bindParam(':phone', $params['phone']);
        $stmt->bindParam(':address', $params['address']);
        $stmt->bindParam(':email', $params['email']);
        $stmt->bindParam(':id', $params['id']);

        return $stmt->execute();
    }

    public function deleteUserData($id) {
        $stmt = $this->db->prepare('DELETE FROM user_data WHERE id = :id');
        $stmt->bindParam(':id', $id);

        return $stmt->execute();
    }
}