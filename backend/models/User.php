<?php

class User {
    public function __construct($db) {
        $this->db = $db;
    }

    public function getAllUsers() {
        $stmt = $this->db->prepare("SELECT * FROM users ORDER BY id DESC");
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getUser($id) {
        $stmt = $this->db->prepare('SELECT * FROM users WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function createUser($params) {
        $stmt = $this->db->prepare('INSERT INTO users (full_name, birth_date) VALUES (:full_name, :birth_date)');
        $stmt->bindParam(':full_name', $params['full_name']);
        $stmt->bindParam(':birth_date', $params['birth_date']);

        return $stmt->execute();
    }

    public function updateUser($params) {
        $stmt = $this->db->prepare('UPDATE users SET full_name = :full_name, birth_date = :birth_date WHERE id = :id');
        $stmt->bindParam(':full_name', $params['full_name']);
        $stmt->bindParam(':birth_date', $params['birth_date']);
        $stmt->bindParam(':id', $params['id']);

        return $stmt->execute();
    }
    
    public function deleteUser($id) {
        $stmt = $this->db->prepare('DELETE FROM users WHERE id = :id');
        $stmt->bindParam(':id', $id);

        return $stmt->execute();
    }
}